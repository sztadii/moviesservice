export interface MovieDocument {
  docs: Movie[]
  limit: number
  offset: number
  page: number
  pages: number
  total: number
}

export interface Movie {
  _id: string
  title: string
  description?: string
  duration?: number
  rate?: number
  votes: number
  url: string
  imgSrc: string
  lang: Lang
}

export enum Lang {
  polish = 'polish'
}
