export interface Router {
  query: Query
}

interface Query {
  [name: string]: any
}
