import { createGlobalStyle } from 'styled-components'
import reset from 'styled-reset'

export const GlobalStyle = createGlobalStyle`
  ${reset}
  
  body {
    font-family: "Nunito", sans-serif;
  }
  
  a {
    text-decoration: none;
    color: inherit;
  }
`
