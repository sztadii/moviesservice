import * as React from 'react'
import MovieDetails from '../components/MovieDetails/MovieDetails'
import Head from '../components/Head/Head'
import Container from '../components/Container/Container'
import { GlobalStyle } from './Pages.style'

export default function Movie() {
  return (
    <Container>
      <Head title="Movie" />
      <MovieDetails />
      <GlobalStyle />
    </Container>
  )
}
