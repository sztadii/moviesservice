import * as React from 'react'
import MovieList from '../components/MovieList/MovieList'
import Head from '../components/Head/Head'
import Container from '../components/Container/Container'
import Title from '../components/Title/Title'
import { GlobalStyle } from './Pages.style'

export default function Home() {
  return (
    <Container>
      <Head title="Home" />
      <Title>All latest movies</Title>
      <MovieList />
      <GlobalStyle />
    </Container>
  )
}
