import { Movie, MovieDocument } from '../types/Movie'

export async function fetchMovieByTitle(title: string): Promise<Movie> {
  const data = await fetch(`/api/movies/${title}`)
  return data.json()
}

export async function fetchAllMovie({ page }): Promise<MovieDocument> {
  const data = await fetch(`/api/movies?page=${page}`)
  return data.json()
}

export async function updateMovies(): Promise<boolean> {
  const data = await fetch('/api/movies/update', {
    method: 'POST'
  })
  return data.ok
}
