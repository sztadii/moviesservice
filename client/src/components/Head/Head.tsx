import * as React from 'react'
import NextHead from 'next/head'

interface HeadProps {
  title?: string
  description?: string
  url?: string
  ogImage?: string
}

export default function Head(props: HeadProps) {
  return (
    <NextHead>
      <meta charSet="UTF-8" />
      <meta name="description" content={props.description} />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta property="og:url" content={props.url} />
      <meta property="og:title" content={props.title} />
      <meta property="og:description" content={props.description} />
      <meta name="twitter:site" content={props.url} />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:image" content={props.ogImage} />
      <meta property="og:image" content={props.ogImage} />
      <meta property="og:image:width" content="1200" />
      <meta property="og:image:height" content="630" />

      <link rel="icon" href="/static/favicon.ico" />
      <link
        href="https://fonts.googleapis.com/css?family=Nunito:400,700"
        rel="stylesheet"
      />
      <title>{props.title}</title>
    </NextHead>
  )
}
