import styled from 'styled-components'

const Loader = styled.div`
  box-sizing: border-box;
  position: absolute;
  top: calc(50% - 30px);
  left: calc(50% - 30px);
  width: 60px;
  height: 60px;
  border-radius: 50%;
  border: 4px solid #b7b7b7;
  border-top-color: #1a183b;
  animation: spinner 0.6s linear infinite;

  @keyframes spinner {
    to {
      transform: rotate(360deg);
    }
  }
`

export default Loader
