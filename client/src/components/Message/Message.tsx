import styled from 'styled-components'

const Message = styled.div`
  background: #404040;
  color: white;
  padding: 20px;
  border-radius: 5px;
  box-shadow: 0 5px 20px #bbbbbb;
  text-align: center;
  cursor: pointer;
`

export default Message
