import styled from 'styled-components'

export const Container = styled.div`
  margin-bottom: 20px;
  padding: 20px;
  box-shadow: 0 5px 20px #bbbbbb;
  border-radius: 5px;
`

export const Flex = styled.div`
  display: flex;
`

export const Title = styled.h1`
  font-size: 18px;
  text-transform: uppercase;
  font-weight: bold;
  margin-bottom: 20px;
`

export const SubTitle = styled.h2`
  display: inline-block;
  text-transform: uppercase;
  font-weight: bold;
`

export const Line = styled.div`
  margin-bottom: 5px;
`

export const ImgBox = styled.div`
  width: 33%;
`

export const Img = styled.img`
  position: relative;
  height: 100px;
  width: 70px;
  background: #e6e4e4;
`

export const Details = styled.div`
  margin-top: 20px;
  padding-top: 30px;
  position: relative;

  &:before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    height: 10px;
    width: 100%;
    max-width: 300px;
    background: #2c75ff;
    border-radius: 8px;
  }
`

export const Desc = styled.div`
  margin-top: 10px;
  font-style: italic;
`
