import * as React from 'react'
import {
  Container,
  Title,
  SubTitle,
  Line,
  Img,
  Flex,
  ImgBox,
  Details,
  Desc
} from './CardDetails.style'
import { Movie } from '../../types/Movie'

export default class CardDetails extends React.PureComponent<Movie> {
  render() {
    const { title, rate, duration, votes, imgSrc, description } = this.props
    return (
      <Container>
        <Title>{title}</Title>
        <Flex>
          <ImgBox>
            <Img src={imgSrc} />
          </ImgBox>
          <div>
            <Line>
              {rate && (
                <div>
                  <SubTitle>Rate:</SubTitle> {rate} stars
                </div>
              )}
            </Line>
            <Line>
              {duration && (
                <div>
                  <SubTitle>Duration:</SubTitle> {duration} min
                </div>
              )}
            </Line>
            <Line>
              <SubTitle>Votes:</SubTitle> {votes} votes
            </Line>
          </div>
        </Flex>
        {description && (
          <Details>
            <SubTitle>Description</SubTitle>
            <Desc>{description}</Desc>
          </Details>
        )}
      </Container>
    )
  }
}
