import styled from 'styled-components'

interface WrapperProps {
  show: boolean
}

export const Wrapper = styled.div`
  position: fixed;
  bottom: 20px;
  right: 20px;
  background: #404040;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 50%;
  width: 40px;
  height: 40px;
  box-shadow: 0 5px 20px #bbbbbb;
  cursor: pointer;
  transition: 0.4s;
  opacity: ${(props: WrapperProps) => (props.show ? 1 : 0)};
`

export const Img = styled.img`
  width: 20px;
  height: 20px;
  transform: rotate(180deg);
`
