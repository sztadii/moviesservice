import * as React from 'react'
import { Wrapper, Img } from './ScrollTop.style'

interface ScrollTopProps {
  show: boolean
  onClick?: (e: React.MouseEvent<HTMLInputElement>) => void
}

export default class ScrollTop extends React.PureComponent<ScrollTopProps> {
  render() {
    const { show, onClick } = this.props
    return (
      <Wrapper show={show} onClick={onClick}>
        <Img src="/static/icon.svg" />
      </Wrapper>
    )
  }
}
