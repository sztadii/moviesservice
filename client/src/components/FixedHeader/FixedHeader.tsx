import styled from 'styled-components'

interface FixedHeaderProps {
  show: boolean
}

const FixedHeader = styled.div`
  position: fixed;
  top: 0;
  right: 0;
  width: 100%;
  padding: 10px;
  background: white;
  box-shadow: 0 5px 20px #bbbbbb;
  transition: 0.4s;
  z-index: 1;
  text-align: center;
  text-transform: uppercase;
  font-weight: bold;
  opacity: ${(props: FixedHeaderProps) => (props.show ? 1 : 0)};
  transform: ${(props: FixedHeaderProps) =>
  props.show ? 'translateY(0)' : 'translateY(-100px)'};
`

export default FixedHeader
