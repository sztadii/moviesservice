import * as React from 'react'
import { withRouter } from 'next/router'
import { fetchMovieByTitle } from '../../services/MovieServie'
import { Movie } from '../../types/Movie'
import { Router } from '../../types/Router'
import Title from '../Title/Title'
import CardDetails from '../CardDetails/CardDetails'
import Loader from '../Loader/Loader'
import Message from '../Message/Message'

interface MovieDetailsState {
  movie: Movie
  loading: boolean
  error: string
}

interface MovieDetailsProps {
  router: Router
}

class MovieDetails extends React.PureComponent<
  MovieDetailsProps,
  MovieDetailsState
> {
  constructor(props: any) {
    super(props)
    this.state = {
      movie: null,
      loading: true,
      error: ''
    }
  }

  componentDidMount() {
    this.getMovie()
  }

  getMovie = async () => {
    try {
      const { query } = this.props.router
      const movie = await fetchMovieByTitle(query.title)
      this.setState({
        movie,
        loading: false
      })
    } catch (e) {
      this.setState({ loading: false, error: e.message })
    }
  }

  renderMovie = () => {
    const { movie } = this.state
    return (
      <div>
        <Title>{movie.title}</Title>
        <CardDetails {...movie} />
      </div>
    )
  }

  render() {
    const { movie, loading, error } = this.state
    if (loading) return <Loader />
    return (
      <div>
        {error && <Message>{error}</Message>}
        {movie && this.renderMovie()}
      </div>
    )
  }
}

export default withRouter(MovieDetails)
