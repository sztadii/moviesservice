import styled from 'styled-components'

const Title = styled.h1`
  font-size: 30px;
  border-radius: 8px;
  padding-left: 20px;
  text-transform: uppercase;
  font-weight: bold;
  margin-bottom: 20px;
  position: relative;

  &:after {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 8px;
    height: 100%;
    background: #2c75ff;
    border-radius: 8px;
  }
`

export default Title
