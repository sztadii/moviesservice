import * as React from 'react'
import Link from 'next/link'
import debounce from 'lodash/debounce'
import { fetchAllMovie, updateMovies } from '../../services/MovieServie'
import { Movie } from '../../types/Movie'
import FixedHeader from '../FixedHeader/FixedHeader'
import Card from '../Card/Card'
import Loader from '../Loader/Loader'
import Message from '../Message/Message'
import Button from '../Button/Button'
import ScrollTop from '../ScrollTop/ScrollTop'

interface MovieListState {
  movies: Movie[]
  loading: boolean
  error: string
  page: number
  isDone: boolean
  total: number
  scrollY: number
}

export default class MovieList extends React.PureComponent<{}, MovieListState> {
  constructor(props: any) {
    super(props)
    this.state = {
      movies: [],
      loading: true,
      error: '',
      page: 1,
      isDone: false,
      total: null,
      scrollY: 0
    }
  }

  async componentDidMount() {
    window.addEventListener('scroll', debounce(this.handleMoviesScroll, 150))
    window.addEventListener('scroll', this.handleScroll)
    this.getMovies()
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleMoviesScroll)
  }

  handleScroll = () => {
    const { scrollY } = window
    this.setState({ scrollY })
  }

  handleScrollClick() {
    window.scrollTo(0, 0)
  }

  handleMoviesScroll = async () => {
    const { innerHeight, scrollY } = window
    const { clientHeight } = document.body
    const positionBottom = innerHeight + scrollY
    if (positionBottom > clientHeight - innerHeight) {
      this.getMovies()
    }
    this.setState({ scrollY })
  }

  async handleUpdateMovies() {
    try {
      await updateMovies()
    } catch (e) {
      this.setState({ error: e.message })
    }
  }

  isNotOnTopPage = (): boolean => {
    const { scrollY } = window
    const gapHeight = 100
    return scrollY > gapHeight
  }

  shouldDisplayScrollButton = (): boolean => {
    const { innerHeight, scrollY } = window
    const { clientHeight } = document.body
    const { movies, isDone } = this.state
    const topAndBottomGapHeight = 100
    const isOnDownPage =
      scrollY + innerHeight + topAndBottomGapHeight > clientHeight && isDone
    return !!movies.length && !isOnDownPage && this.isNotOnTopPage()
  }

  async getMovies() {
    const { movies, page, isDone } = this.state
    if (isDone) return null
    try {
      const { docs, total } = await fetchAllMovie({ page })
      const isDone = !docs.length
      this.setState({
        movies: [...movies, ...docs],
        loading: false,
        page: page + 1,
        isDone,
        total
      })
    } catch (e) {
      this.setState({ loading: false, error: e.message })
    }
  }

  render() {
    const { movies, loading, error, isDone, total } = this.state
    if (loading) return <Loader />
    return (
      <div>
        <FixedHeader show={this.isNotOnTopPage()}>
          All {total} movies
        </FixedHeader>
        {!error && !movies.length && (
          <div>
            <Message>We do not have any movies to display</Message>
            <Button onClick={this.handleUpdateMovies}>Trigger scheduler</Button>
          </div>
        )}
        {error && <Message>{error}</Message>}
        {movies.map(movie => (
          <Link key={movie.title} href={`/movie?title=${movie.title}`}>
            <a>
              <Card {...movie} />
            </a>
          </Link>
        ))}
        {!!movies.length && isDone && (
          <Message onClick={this.handleScrollClick}>
            You fetched all {total} movies, scroll up
          </Message>
        )}
        <ScrollTop
          show={this.shouldDisplayScrollButton()}
          onClick={this.handleScrollClick}
        />
      </div>
    )
  }
}
