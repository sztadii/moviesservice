import styled from 'styled-components'

const Button = styled.button`
  background: #2c75ff;
  color: white;
  padding: 10px 20px;
  border-radius: 20px;
  box-shadow: 0 5px 20px #bbbbbb;
  margin: 10px auto;
  display: block;
  width: 200px;
  outline: none;
  cursor: pointer;
`

export default Button
