// It prevent to use many package manager (yarn, bower) in one project
const isNpm = process.env.npm_execpath.includes('npm')

if (!isNpm) {
  console.log('=========================================')
  console.log('Please install / add dependencies via npm')
  console.log('Other managers are not allowed')
  console.log('=========================================')
  process.exit(1)
}
