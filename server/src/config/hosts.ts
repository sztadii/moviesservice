export default {
  filmWeb: 'https://www.filmweb.pl',
  filmWebSingleMovie: 'https://www.filmweb.pl/film',
  filmWebPremiers: 'https://www.filmweb.pl/ranking/premiere',
  filmWebWanted: 'https://www.filmweb.pl/ranking/wantToSee/next90daysWorld'
}
