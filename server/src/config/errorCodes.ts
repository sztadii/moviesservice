export default {
  ok: 200,
  unprocessableEntity: 422
}
