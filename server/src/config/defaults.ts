export const DB_URI =
  process.env.MONGO_DB_URI || 'mongodb://localhost:27017/movies'
export const SERVER_PORT = 3000
