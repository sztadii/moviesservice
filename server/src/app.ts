import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as cors from 'cors'
import movies from './controller/MovieController'
import test from './controller/TestController'
import MoviesScheduler from './scheduler/MovieScheduler'
const app = express()

app.use(bodyParser.json())
app.use(cors())

app.use('/api/movies', movies)
app.use('/api/test', test)

MoviesScheduler()

export default app
