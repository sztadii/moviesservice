import * as schedule from 'node-schedule'
import * as MovieService from '../service/MovieService'

export default function MovieScheduler() {
  schedule.scheduleJob(
    { minute: 8 }, //It means 10:08, 11:08, 12:08 etc
    MovieService.updateUniqMovies
  )
}
