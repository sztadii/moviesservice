export interface Movie {
  title: string
  description?: string
  duration: number
  rate?: number
  votes: number
  url: string
  imgSrc: string
  lang: Lang
}

export enum Lang {
  polish = 'polish'
}
