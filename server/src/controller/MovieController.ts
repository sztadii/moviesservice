import * as express from 'express'
import * as moviesService from '../service/MovieService'
import errorCodes from '../config/errorCodes'
const router = express.Router()

router.get('/', async (req, res) => {
  try {
    const { limit, page } = req.query
    res.json(await moviesService.getAllMovies(limit, page))
  } catch (e) {
    res.status(errorCodes.unprocessableEntity).send(e.message)
  }
})

router.post('/', async (req, res) => {
  try {
    res.json(await moviesService.createMovie(req.body))
  } catch (e) {
    res.status(errorCodes.unprocessableEntity).send(e.message)
  }
})

router.delete('/', async (req, res) => {
  try {
    res.json(await moviesService.deleteAllMovies())
  } catch (e) {
    res.status(errorCodes.unprocessableEntity).send(e.message)
  }
})

router.get('/:title', async (req, res) => {
  try {
    res.json(await moviesService.getMovieByTitle(req.params.title))
  } catch (e) {
    res.status(errorCodes.unprocessableEntity).send(e.message)
  }
})

router.post('/update', (req, res) => {
  // Here we will trigger below function
  // We will send response, but on possible error we will display it
  moviesService.updateUniqMovies().catch(e => console.error(e))
  res.status(errorCodes.ok).send()
})

export default router
