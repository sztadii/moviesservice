import * as express from 'express'
const router = express.Router()

router.get('/', async (req, res) => {
  res.json('TestController is working')
})

export default router
