import { getPremierMovies, getWantedMovies } from './FilmWebService'
import {
  nockFilmWebPremiers,
  nockSingleMovies,
  nockFilmWebWanted
} from './nocks'

test('getPremierMovies return list of movies from FilmWeb.pl', async () => {
  nockFilmWebPremiers()
  nockSingleMovies()
  const data = await getPremierMovies()
  expect(data).toMatchSnapshot()
})

test('getWantedMovies return list of movies from FilmWeb.pl', async () => {
  nockFilmWebWanted()
  nockSingleMovies()
  const data = await getWantedMovies()
  expect(data).toMatchSnapshot()
})
