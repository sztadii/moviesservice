import * as nock from 'nock'
import hosts from '../config/hosts'

export function nockFilmWebPremiers() {
  nock(hosts.filmWebPremiers)
    .get('')
    .replyWithFile(200, __dirname + '/response/filmWebPremiersResponse.html')
}

// persist() allow infinite repeated responses, by default we can mock only once
export function nockSingleMovies() {
  nock(hosts.filmWebSingleMovie)
    .persist()
    .get(uri => uri.includes('/film'))
    .replyWithFile(200, __dirname + '/response/filmWebSingleMovieResponse.html')
}

export function nockFilmWebWanted() {
  nock(hosts.filmWebWanted)
    .get('')
    .replyWithFile(200, __dirname + '/response/filmWebWantedResponse.html')
}
