import { JSDOM } from 'jsdom'
import axios from 'axios'
import hosts from '../config/hosts'
import { Movie, Lang } from '../types/Movie'

export async function getPremierMovies(): Promise<Movie[]> {
  const document = await getDOMFromURL(hosts.filmWebPremiers)
  const rows = [
    ...document.querySelector('.ranking__list').querySelectorAll('.place')
  ]
  const movies = rows
    .map(row => {
      const movieLink = row.querySelector('.film__link')
      const rateValue = row.querySelector('.rate__value')
      const rateCount = row.querySelector('.rate__count')
      const img = row.querySelector('img')
      return {
        title: movieLink && movieLink.textContent.trim(),
        rate: rateValue && getNumberFromElement(rateValue),
        votes: rateCount && parseInt(rateCount.innerHTML),
        url: movieLink && getUrlFromElement(movieLink),
        imgSrc: img && getImgSrcFromElement(img),
        lang: Lang.polish
      }
    })
    .filter(item => item.title)
    .map(async movie => {
      return {
        ...movie,
        ...(await getMovieDetailsFromURL(movie.url))
      }
    })
  return Promise.all(movies)
}

export async function getWantedMovies(): Promise<Movie[]> {
  const document = await getDOMFromURL(hosts.filmWebWanted)
  const rows = [
    ...document.querySelector('.ranking__list').querySelectorAll('.place')
  ]
  const movies = rows
    .map(row => {
      const movieLink = row.querySelector('.film__link')
      const count = row.querySelector('.film__wts-count')
      const img = row.querySelector('img')
      return {
        title: movieLink && movieLink.textContent.trim(),
        votes: count && parseInt(count.innerHTML),
        url: movieLink && getUrlFromElement(movieLink),
        imgSrc: img && getImgSrcFromElement(img),
        lang: Lang.polish
      }
    })
    .filter(item => item.title)
    .map(async movie => {
      return {
        ...movie,
        ...(await getMovieDetailsFromURL(movie.url))
      }
    })
  return Promise.all(movies)
}

async function getDOMFromURL(url) {
  const { data } = await axios.get(url)
  console.error = () => {}
  return new JSDOM(data).window.document
}

async function getMovieDetailsFromURL(url: string) {
  const document = await getDOMFromURL(url)
  return {
    description: getDescriptionFromDocument(document),
    duration: getDurationFromDocument(document)
  }
}

function getUrlFromElement(element: Element): string {
  return hosts.filmWeb + element.getAttribute('href')
}

function getNumberFromElement(element: Element): number {
  const numeric = element.innerHTML.replace(',', '.')
  const value = Number(numeric)
  return isNaN(value) ? null : value
}

function getImgSrcFromElement(element: Element): string {
  return element.getAttribute('src') || element.getAttribute('data-src')
}

function getDescriptionFromDocument(document: Document): string {
  const desc = document
    .querySelector('.text')
    .textContent.replace('czytaj dalej', '')
    .trim()
  return desc.includes('Ten film nie ma jeszcze zarysu') ? null : desc
}

function getDurationFromDocument(document: Document): number {
  const durationElement = document.querySelector('.filmTime')
  if (!durationElement) return null
  const [hour = 0, minute = 0] = durationElement.textContent
    .match(/(\d)+/g)
    .map(Number)
  return hour * 60 + minute
}
