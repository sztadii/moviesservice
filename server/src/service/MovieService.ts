import Movie from '../model/Movie'
import * as FilmWebMovies from './FilmWebService'

export function getAllMovies(limit = 10, page = 1) {
  const options = {
    sort: {
      createdAt: -1
    },
    limit,
    page
  }
  return Movie.paginate({}, options)
}

export function createMovie(movie) {
  return new Movie(movie).save()
}

export function createManyMovies(movies) {
  return Movie.insertMany(movies)
}

export function deleteAllMovies() {
  return Movie.remove({})
}

export function getMovieByTitle(title) {
  return Movie.findOne({ title })
}

export async function updateUniqMovies() {
  const wanted = await FilmWebMovies.getWantedMovies()
  const premier = await FilmWebMovies.getPremierMovies()
  await deleteAllMovies()
  await createManyMovies([...wanted, ...premier])
}
