import { Schema, model, PaginateModel, Document } from 'mongoose'
import mongoosePaginate = require('mongoose-paginate')
import { Movie as IMovie } from '../types/Movie'

interface Movie extends IMovie, Document {}
interface MovieModel<T extends Document> extends PaginateModel<T> {}

const MovieSchema: Schema = new Schema(
  {
    title: String,
    description: String,
    duration: Number,
    rate: {
      type: Number,
      default: null
    },
    votes: Number,
    url: String,
    imgSrc: String,
    lang: String
  },
  {
    timestamps: {
      createdAt: true,
      updatedAt: true
    }
  }
)

MovieSchema.plugin(mongoosePaginate)

const MovieModel: MovieModel<Movie> = model<Movie>(
  'Movie',
  MovieSchema
) as MovieModel<Movie>

export default MovieModel
