# movies_service
> Full stack movies service based on docker containers

### Description

* server - backend service written in TS, which downloads html from other websites (in a cyclical manner) and parses it to JSON and writes it to the mongodb model
* client - frontend client written in TS, which present our movie's content

### Development setup

* Install and run Docker
* Build app `./script build_image`
* Run app in development mode `./script run_development`
* Open app on `http://localhost:8080`

### Production setup

* Install and run Docker
* Run one below command
* Build app `./script build_image`
* Run app in production mode `./script run_production`
* Open app on `http://localhost:8080`

### Other commands

* Build and run app in development mode `./script build_image run_development`
* Build and run app in production mode `./script build_image run_production`
